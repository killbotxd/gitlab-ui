# Contributing to GitLab UI

Here are a few resources to guide you through contributing to GitLab UI.

- [Adding new components](./doc/contributing/adding_components.md)
- [Adding CSS](./doc/contributing/adding_css.md)
- [Commit message conventions and rules](./doc/contributing/commits.md)
- [Testing locally](./doc/contributing/local_gitlab_testing.md)
- [Automatic components documentation](./doc/contributing/automatic_documentation.md)
- [Documentation guidelines](./doc/contributing/documentation_guidelines.md)
- [Troubleshooting](./doc/contributing/troubleshooting.md)

Questions? Head to our [FAQ](./FAQ.md) where you might find some answers.

## GitLab's contribution guidelines

Please refer to [gitlab's CONTRIBUTING.md](https://gitlab.com/gitlab-org/gitlab/blob/master/CONTRIBUTING.md) for details on our guidelines.

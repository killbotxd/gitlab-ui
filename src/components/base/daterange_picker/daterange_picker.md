# Daterange Picker

<!-- STORY -->

## Usage

Daterange picker allows users to choose a date range by manually typing the start/end date into the input fields or by using a calendar-like dropdown.

A maxDateRange can be specified in order to limit the maximum number of days the component will allow to be selected i.e. if the start date is 2020-08-01 and maxDateRange is set to 10, the highest selectable end date would be 2020-08-11.

### Note

If specifying a maxDateRange, it is good practice to include a date range indicator and toolip.

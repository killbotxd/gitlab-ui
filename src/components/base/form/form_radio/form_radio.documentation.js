import description from './form_radio.md';
import examples from './examples';

export default {
  description,
  examples,
  followsDesignSystem: true,
  bootstrapComponent: 'b-form-radio',
};
